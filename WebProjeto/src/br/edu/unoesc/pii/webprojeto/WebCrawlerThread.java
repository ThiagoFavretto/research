package br.edu.unoesc.pii.webprojeto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import br.edu.unoesc.pii.crial.Crial;
import br.edu.unoesc.pii.jweb.JSplash;
import br.gov.i3gov.core.CrawlerListener;
import br.gov.i3gov.crawler.links.BufferLinks;
import br.gov.i3gov.crawler.links.Link;
import br.gov.i3gov.crawler.links.LinkImpl;
import br.gov.i3gov.crawler.links.RAMQueueBufferLinks;
import br.gov.i3gov.crawler.tools.MultiThreadCrawler;
import br.gov.i3gov.crawler.tools.MultiThreadCrawlerImpl;

public class WebCrawlerThread implements CrawlerListener, Runnable {

	/*
	 * 0 Este m�todo deve ser implementado pela classe que escuta o crawler
	 *
	 * A cada p�gina processada pelo crawler este m�todo � chamado.
	 */
	public int cont = 1;
	public int cont2;
	private JSplash splash;

	/**
	 * @return the splash
	 */
	public JSplash getSplash() {
		return splash;
	}

	/**
	 * @param splash
	 *            the splash to set
	 */
	public void setSplash(JSplash splash) {
		this.splash = splash;
	}

	public void processPage(Page page, Link link) {
		// 0 cria/escreve
		FileWriter arquivo;
		try {
			arquivo = new FileWriter(new File("arquivo.txt"), true);
			arquivo.write(((HtmlPage) page).asText());
			arquivo.write("\n");
			arquivo.write(link.getUrl());
			arquivo.write("\n");
			arquivo.write("=======" + cont + "\n");
			cont++;
			arquivo.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//
		cont2 = 1;
		try {
			boolean flag = false;

			Scanner scn = new Scanner(new File("arquivo.txt"));
			FileWriter arbruto;
			arbruto = new FileWriter(new File("arbruto.txt"));
			while (scn.hasNextLine()) {
				String linha = scn.nextLine();

				if (!(linha.startsWith("====="))) {
					// EXCLUI
					if (!(linha.endsWith("..")) && !(linha.endsWith(")")) && !(linha.endsWith("Americanas.com"))
							&& !(linha.endsWith("recomendoPrado"))) {
						// INCLUI
						if ((linha.contains("GB")) || (linha.contains("gb")) || (linha.contains("HD"))
								|| (linha.contains("Hd")) || (linha.contains("ghz")) || (linha.contains("GHZ"))
								|| (linha.contains("Tb")) || (linha.contains("TB")) || (linha.contains("R$"))
								|| (linha.contains("http"))) {
							if (linha.startsWith("http")) {
								arbruto.write(linha + "\r");
								arbruto.write("\n");
							}
							if (linha.startsWith(Crial.produto)) {
								flag = true;

								arbruto.write(linha + "\r");
								arbruto.write("\n");

							}
							if (linha.startsWith("R$ ") && (flag == true)) {
								flag = false;
								arbruto.write(linha + "\r");
								arbruto.write("\n");
								arbruto.write("\n");
							}
						}

					}
				} else {

					arbruto.write("**********" + cont2 + "\r");
					arbruto.write("\n");
					cont2++;

				}
			}
			arbruto.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//
		link.getUrl();
		/* Imprime a url, o tipo e o conte�do das p�ginas html visitadas */
		System.out.println(link.getUrl());
		// System.out.println(link.getContentType());
		// System.out.println(((HtmlPage)page).asText());
		// System.out.println("*********************************************");

		link.getUrl();
		link.getContentType();

	}

	// =====================================================================================
	@Override
	public void run() {
		try {
			FileWriter arquivo = new FileWriter(new File("arquivo.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebCrawlerThread teste = new WebCrawlerThread();

		/* Cria link inicial para a navega��o web */
		Link link = new LinkImpl();
		link.setUrl(Crial.site);
		link.setDepth(new Long(0));
		/* Cria a buffer que cont�m os links a serem visitados */
		BufferLinks bufferLinks = new RAMQueueBufferLinks();

		/* Adiciona ao buffer o(s) link(s) iniciais para a navega��o na web */
		bufferLinks.push(link);

		/*
		 * express�o regular que indica quais urls devem ser navegadas. Nesse caso todas
		 * as urls deste dom�nio ser�o visitadas
		 */
		bufferLinks.addFiltro(Crial.par);
		/* Criar o crawler de busca e adiciona a ele o buffer a ser utilizado */
		MultiThreadCrawler crawler = new MultiThreadCrawlerImpl();

		crawler.setBufferLinks(bufferLinks);

		/* Seta o n�mero de threads usadas pelo crawler */
		crawler.setMaxThreads(32);

		/*
		 * Adiciona a classe ouvinte que ser� respons�vel por receber as p�ginas
		 * visitadas
		 */
		crawler.addCrawlerListener(teste);

		/*
		 * Adiciona o tipo de p�gina a ser visitado pelo crawler. O default � que todos
		 * os tipos sejam visistados
		 */
		crawler.addFilterContentType("text/html");

		/* Inicia o crawler */
		crawler.startCrawler();

		if (splash != null) {
			splash.setVisible(false);

		}
	}

}
