package br.edu.unoesc.pii.crial;

import br.edu.unoesc.pii.jweb.Janela;

public class Crial extends Janela {
	public static String site;
	public static String par;
	public static String produto;

	public void link() {

		if (Janela.kbu) {
			if (Janela.generico) {
				site = "https://www.kabum.com.br/computadores/computadores";
				par = "https://www.kabum.com.br/produto/.*";
				produto = "Computador ";
			} else if (Janela.not) {
				site = "https://www.kabum.com.br/computadores/notebooks-ultrabooks";
				par = "https://www.kabum.com.br/produto/.*";
				produto = "Notebook ";
			} else if (Janela.gamer) {
				site = "https://www.kabum.com.br/computadores/computador-gamer";
				par = "https://www.kabum.com.br/produto/.*";
				produto = "Computador ";
			}
		} else {
			if (Janela.generico) {
				site = "https://www.americanas.com.br/categoria/informatica/computadores-e-all-in-one/computador";
				par = "https://www.americanas.com.br/produto/.*";
				produto = "Computador ";
			} else if (Janela.not) {
				site = "https://www.americanas.com.br/categoria/informatica/notebook";
				par = "https://www.americanas.com.br/produto/.*";
				produto = "Notebook ";
			} else if (Janela.gamer) {
				site = "https://www.americanas.com.br/categoria/informatica/computadores-e-all-in-one/computador-gamer";
				par = "https://www.americanas.com.br/produto/.*";
				produto = "Computador ";
			}
		}

	}
}
