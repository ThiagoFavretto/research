package br.edu.unoesc.pii.jweb;

import java.awt.Color;
import java.awt.Font;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import br.edu.unoesc.pii.crial.Crial;
import br.edu.unoesc.pii.webprojeto.WebCrawlerThread;
import javax.swing.JTextField;
import java.awt.Cursor;

public class Janela extends JFrame {

	private JPanel contentPane;
	public JTextArea textArea;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	public TextField textPag;
	public static boolean amz = false;
	public static boolean kbu = true;
	public static boolean generico = true;
	public static boolean all = false;
	public static boolean gamer = false;
	public static boolean not = false;
	private JTextField textField;
	private JTextField txtDisponivelEm;
	public int cont = 2;
	public int cont2 = 1;

	public Janela() {
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		setTitle("REsearch");
		setResizable(false);
		setBackground(Color.WHITE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Janela.class.getResource("/img/LogoL.PNG")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 753, 313);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		// buscar
		JButton btnBusca = new JButton("");
		btnBusca.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnBusca.setEnabled(true);
		btnBusca.setForeground(Color.BLACK);
		btnBusca.setIcon(new ImageIcon(Janela.class.getResource("/img/searchs.png")));
		btnBusca.setBackground(Color.WHITE);
		btnBusca.setFont(new Font("Arial", Font.PLAIN, 16));
		btnBusca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Crial gera = new Crial();
				gera.link();
				textArea.setText("");
				WebCrawlerThread crawler = new WebCrawlerThread();
				Thread t = new Thread(crawler);
				t.start();
				JSplash Jjanela = null;
				while (t.isAlive() && Jjanela == null) {
					Jjanela = new JSplash();
					Jjanela.setVisible(true);
					crawler.setSplash(Jjanela);
				}
                                                    
                                                 	   

			}
		});
		btnBusca.setBounds(2, 233, 110, 39);
		contentPane.add(btnBusca);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 747, 21);
		contentPane.add(menuBar);

		JMenu mnOpes = new JMenu("Op\u00E7\u00F5es");
		menuBar.add(mnOpes);

		JMenuItem mntmSair = new JMenuItem("Sair      ");
		mntmSair.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// saindo do sistema
				dispose();// fecha o formul�rio
				System.exit(0);// termina a aplica��o
			}
		});
		mntmSair.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		mnOpes.add(mntmSair);
		// next
		JButton btnProx = new JButton("");
		btnProx.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnProx.setForeground(Color.BLACK);
		btnProx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
				try {
					Scanner scn = new Scanner(new File("arbruto.txt"));
					boolean flag = false;
					
					while (scn.hasNextLine()) {
						String linha = scn.nextLine();
						if (linha.startsWith("http")) {
							textField.setText("");
							textField.setText(linha);
						}
						
						if (linha.startsWith("**********" + cont)) {
							textPag.setText("Pagina " + cont);
							cont++;
							cont2++;
														break;
						} else {
							if (linha.endsWith("**********" + cont2))
								flag = true;
						}

						if (flag) {
							if (!(linha.startsWith("********"))&& !(linha.startsWith("http"))) {
								textArea.append(linha + "\n");
							}
						}

					}

				} catch (IOException ioe) {
					ioe.printStackTrace();
				}

			}
		});

		btnProx.setBackground(Color.WHITE);
		btnProx.setIcon(new ImageIcon(Janela.class.getResource("/img/next.png")));
		btnProx.setFont(new Font("Arial", Font.PLAIN, 16));
		btnProx.setBounds(120, 233, 113, 39);
		contentPane.add(btnProx);
		textPag = new TextField();
		textPag.setEditable(false);
		textPag.setBounds(651, 250, 62, 22);
		contentPane.add(textPag);
		textPag.setFont(new Font("Arial", Font.PLAIN, 13));
		textPag.setText("Pagina 0");

		JPanel panel = new JPanel();
		panel.setForeground(Color.BLACK);
		panel.setBackground(Color.DARK_GRAY);
		panel.setBorder(new TitledBorder(
				new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Resultado",
				TitledBorder.LEFT, TitledBorder.TOP, null, Color.WHITE));
		panel.setBounds(236, 21, 511, 261);
		contentPane.add(panel);
		panel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 21, 491, 199);
		panel.add(scrollPane);

		textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(98, 230, 300, 20);
		panel.add(textField);
		textField.setColumns(10);

		txtDisponivelEm = new JTextField();
		txtDisponivelEm.setEditable(false);
		txtDisponivelEm.setText("Disponivel em:");
		txtDisponivelEm.setBounds(10, 230, 86, 20);
		panel.add(txtDisponivelEm);
		txtDisponivelEm.setColumns(10);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.DARK_GRAY);
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Site", TitledBorder.CENTER,
				TitledBorder.TOP, null, Color.WHITE));
		panel_1.setBounds(0, 20, 237, 69);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JRadioButton rdbtnSite2 = new JRadioButton("Americanas");
		rdbtnSite2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup.add(rdbtnSite2);
		rdbtnSite2.setForeground(Color.WHITE);
		rdbtnSite2.setBounds(114, 29, 117, 33);
		panel_1.add(rdbtnSite2);
		rdbtnSite2.setBackground(Color.DARK_GRAY);
		rdbtnSite2.setFont(new Font("Arial", Font.PLAIN, 16));
		rdbtnSite2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				amz = true;
				kbu = false;

			}
		});

		JRadioButton rdbtnSite1 = new JRadioButton("Kabum");
		rdbtnSite1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup.add(rdbtnSite1);
		rdbtnSite1.setForeground(Color.WHITE);
		rdbtnSite1.setBounds(6, 29, 85, 33);
		panel_1.add(rdbtnSite1);
		rdbtnSite1.setBackground(Color.DARK_GRAY);
		rdbtnSite1.setFont(new Font("Arial", Font.PLAIN, 16));
		rdbtnSite1.setSelected(true);
		rdbtnSite1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				amz = false;
				kbu = true;

			}
		});

		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Computadores",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(255, 255, 255)));
		panel_2.setBackground(Color.DARK_GRAY);
		panel_2.setBounds(0, 89, 237, 109);
		contentPane.add(panel_2);

		JRadioButton rdbtnTipo1 = new JRadioButton("Generico");
		rdbtnTipo1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		rdbtnTipo1.setSelected(true);
		buttonGroup_1.add(rdbtnTipo1);
		rdbtnTipo1.setForeground(Color.WHITE);
		rdbtnTipo1.setBounds(6, 17, 93, 39);
		panel_2.add(rdbtnTipo1);
		rdbtnTipo1.setBackground(Color.DARK_GRAY);
		rdbtnTipo1.setFont(new Font("Arial", Font.PLAIN, 16));
		rdbtnTipo1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generico = true;
				all = false;
				gamer = false;
				not = false;
			}
		});

		JRadioButton rdbtnTipo2 = new JRadioButton("All In One");
		rdbtnTipo2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup_1.add(rdbtnTipo2);
		rdbtnTipo2.setForeground(Color.WHITE);
		rdbtnTipo2.setBounds(115, 17, 93, 39);
		panel_2.add(rdbtnTipo2);
		rdbtnTipo2.setBackground(Color.DARK_GRAY);
		rdbtnTipo2.setFont(new Font("Arial", Font.PLAIN, 16));
		rdbtnTipo2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generico = false;
				all = true;
				gamer = false;
				not = false;
			}
		});

		JRadioButton rdbtnTipo3 = new JRadioButton("Gamers");
		rdbtnTipo3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup_1.add(rdbtnTipo3);
		rdbtnTipo3.setForeground(Color.WHITE);
		rdbtnTipo3.setFont(new Font("Arial", Font.PLAIN, 16));
		rdbtnTipo3.setBackground(Color.DARK_GRAY);
		rdbtnTipo3.setBounds(6, 59, 81, 39);
		panel_2.add(rdbtnTipo3);
		rdbtnTipo3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generico = false;
				all = false;
				gamer = true;
				not = false;

			}
		});

		JRadioButton rdbtnTipo4 = new JRadioButton("Notebooks");
		rdbtnTipo4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonGroup_1.add(rdbtnTipo4);
		rdbtnTipo4.setForeground(Color.WHITE);
		rdbtnTipo4.setFont(new Font("Arial", Font.PLAIN, 16));
		rdbtnTipo4.setBackground(Color.DARK_GRAY);
		rdbtnTipo4.setBounds(115, 59, 104, 39);
		panel_2.add(rdbtnTipo4);
		rdbtnTipo4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generico = false;
				all = false;
				gamer = false;
				not = true;

			}
		});
	}
}
