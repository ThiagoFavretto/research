package br.edu.unoesc.pii.jweb;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import java.awt.Color;

public class JAbre extends JFrame {

	private JPanel contentPane;
	private Timer t;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JAbre frame = new JAbre();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JAbre() {
		setTitle("REsearch");
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 223, 242);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(0, 218, 224, 24);
		progressBar.setForeground(Color.DARK_GRAY);
		progressBar.setBackground(Color.LIGHT_GRAY);
		progressBar.setIndeterminate(true);
		contentPane.add(progressBar);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 224, 218);
		lblNewLabel.setIcon(new ImageIcon(JAbre.class.getResource("/img/Logo.png")));
		contentPane.add(lblNewLabel);
		t = new Timer(1500, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Janela Jjanela = new Janela();
				Jjanela.setVisible(true);
				// interrompe o timer
				t.stop();
				// fecha o splash
				dispose();

			}
		});
		t.start();

	}

}
