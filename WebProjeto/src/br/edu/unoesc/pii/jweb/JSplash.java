package br.edu.unoesc.pii.jweb;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JProgressBar;
import java.awt.Rectangle;
import java.awt.Toolkit;

public class JSplash extends JFrame {

	private JPanel contentPane;


	/**
	 * Create the frame.
	 */
	public JSplash() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(JSplash.class.getResource("/img/pesquisa.jpg")));
		setUndecorated(true);
		setBackground(Color.WHITE);
		setTitle("Carregando!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 753, 313);
		contentPane = new JPanel();
		setLocationRelativeTo( null );
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, Color.LIGHT_GRAY, Color.LIGHT_GRAY, Color.LIGHT_GRAY));
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblCarregandoOJogo = new JLabel("Procurando...!");
		lblCarregandoOJogo.setBounds(new Rectangle(0, 128, 0, 0));
		lblCarregandoOJogo.setIcon(new ImageIcon(JSplash.class.getResource("/img/pesquisa.jpg")));
		lblCarregandoOJogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblCarregandoOJogo.setBackground(Color.WHITE);
		lblCarregandoOJogo.setForeground(Color.WHITE);
		lblCarregandoOJogo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		contentPane.add(lblCarregandoOJogo, BorderLayout.CENTER);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		contentPane.add(progressBar, BorderLayout.SOUTH);

	}

}
